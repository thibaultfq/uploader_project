import React, { Component } from 'react';
import Dropzone from 'react-dropzone'
import ToUploadItem from './ToUploadItem'
import './css/ToUploadList.css'


class ToUploadList extends Component {

    constructor() {
        super()
        this.state = { files: [] }
    }

    onDrop = (files) => {
        const newFiles = [...this.state.files];
        files.forEach( (file) => {
            let existingFile = newFiles.find(x => x.name === file.name)
            if (!existingFile) {
                newFiles.push(file);
            } else {
                existingFile = file;
            }
        });

        this.setState({ files: newFiles });

        console.log(this.state.files)

    }

    removeFile = (file) => {
        const files = [...this.state.files]
        files.splice(files.find(x => x === file), 1)
        this.setState({files});

        console.log(this.state.files)
    }

    render() {
        return (
            <div>
                <div className="card-panel teal drop-card">
                    <div className="dropzone" >
                        <Dropzone accept="application/pdf" onDrop={this.onDrop} className="dropzone-inner center" activeClassName="dropzone-active" rejectClassName="dropzone-active-reject">
                            {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
                                if (isDragActive) {
                                    return (
                                        <span className="white-text"><i className="material-icons">add_circle</i> Drop me!</span>
                                    );
                                }
                                if (isDragReject) {
                                    return (<span className="white-text"><i className="material-icons">error</i>unsupported filetype</span>)
                                }
                                return (<span className="white-text"><i className="material-icons">attach_file</i>Click to add files or drag them here</span>)
                            }}
                        </Dropzone>
                    </div>
                </div>
                {this.state.files.map(f => <ToUploadItem key={f.name} file={f} removeFile={this.removeFile}/>)}
            </div>
        );
    }
}



export default ToUploadList;