import React, { Component } from 'react';
import './css/App.css';
import UploadTask from './UploadTask.js'

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
        <UploadTask/>          
        </div>
      </div>
    );
  }
}

export default App;
