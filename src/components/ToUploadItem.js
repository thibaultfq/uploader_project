import React, { Component } from 'react';
import PropTypes from 'prop-types'; 


class ToUploadItem extends Component {
    render() {
        return (
            <div className="chip">
                {this.props.file.name}
                <i onClick={ () => this.props.removeFile(this.props.file)} className="close material-icons">close</i>
            </div>
        );
    }
}

ToUploadItem.PropTypes = {
    file: PropTypes.object.isRequired,
    removeFile: PropTypes.func.isRequired
}

export default ToUploadItem;