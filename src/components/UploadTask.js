import React, { Component } from 'react';
import ToUploadList from './ToUploadList.js'
import './css/ToUploadTask.css';


class UploadTask extends Component {
    addToUploadItem = (event) => {
        console.log(event);
        console.log(this);
        document.getElementById('upload').click();
    }

    render() {
        return (
            <div className="col s12 m6 l4 offset-l4 offset-m3">
                <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                        <span className="card-title">Upload your files here!</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                    </div>
                    <div className="card-action">
                        <ToUploadList />
                        <a className="waves-effect waves-light btn"><i className="material-icons left">file_upload</i>Upload</a>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default UploadTask;