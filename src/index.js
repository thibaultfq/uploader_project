import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import './css/index.css';
import './css/materialize-css-icon.css';
import 'materialize-css/bin/materialize.css'
import 'materialize-css/bin/materialize.js'


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
